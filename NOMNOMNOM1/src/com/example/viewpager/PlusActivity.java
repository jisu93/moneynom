package com.example.viewpager;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class PlusActivity extends Activity {
	myDBHelper myHelper;
	SQLiteDatabase sqlDB;
	EditText setName, setMoney;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plus);
		
		Button buttComplete = (Button) findViewById(R.id.buttComplete);
		setName =  (EditText) findViewById(R.id.setName);
		setMoney =  (EditText) findViewById(R.id.setMoney);
		myHelper = new myDBHelper(this);
		
		buttComplete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				String name = setName.getText().toString();
				String money = setMoney.getText().toString();
				sqlDB = myHelper.getWritableDatabase();
				myHelper.onUpgrade(sqlDB, 1, 2); // 인수는 아무거나 입력하면 됨.
				sqlDB.close();
				
				
				Intent intent = new Intent(PlusActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
	}
	public class myDBHelper extends SQLiteOpenHelper {
		public myDBHelper(Context context) {
			super(context, "groupDB", null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE  groupTBL (name CHAR(20) PRIMARY KEY , money INTEGER, state INTEGER);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS groupTBL");
			onCreate(db);
		}
	}
}
