package com.example.viewpager.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.viewpager.Content;
import com.example.viewpager.PlusActivity;
import com.example.viewpager.R;

public class ListAdapter extends BaseAdapter {
	private Context context;
	private int layout;
	private ArrayList<Content> items;
	private LayoutInflater inflater;

	public ListAdapter(Context context, int layout, ArrayList<Content> items) {
		this.context = context;
		this.layout = layout;
		this.items = items;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(layout, parent, false);
		}


		TextView textName = (TextView) convertView.findViewById(R.id.textName);
		textName.setText(items.get(position).getTitle());
		TextView textMoney = (TextView) convertView.findViewById(R.id.textMoney);
		textName.setText(items.get(position).getTitle());

		Button buttPlus = (Button) convertView.findViewById(R.id.buttPlus);
		buttPlus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, PlusActivity.class);
				context.startActivity(intent);
			}
		});
		return convertView;
	}

}