package com.example.viewpager;

import java.io.Serializable;

public class Content implements Serializable {
	String title;
	int image;
	String btn;

	public String getBtn() {
		return btn;
	}

	public void setBtn(String btn) {
		this.btn = btn;
	}

	public Content(String title, int image, String btn) {
		this.title = title;
		this.image = image;
		this.btn = btn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

}
