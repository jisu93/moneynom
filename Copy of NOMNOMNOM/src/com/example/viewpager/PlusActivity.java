package com.example.viewpager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PlusActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plus);
		
		Button buttComplete = (Button) findViewById(R.id.buttComplete);
		buttComplete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				
				
				Intent intent = new Intent(PlusActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
	}
	public class myDBHelper extends SQLiteOpenHelper {
		public myDBHelper(Context context) {
			super(context, "groupDB", null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE  groupTBL (Num INTEGER PRIMARY KEY, gName CHAR(20) , gNumber INTEGER);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS groupTBL");
			onCreate(db);
		}
	}
}
