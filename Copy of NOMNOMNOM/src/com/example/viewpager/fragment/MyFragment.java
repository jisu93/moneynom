package com.example.viewpager.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.viewpager.Content;
import com.example.viewpager.R;
import com.example.viewpager.adapter.ListAdapter;

public class MyFragment extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";

	public MyFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		Bundle args = getArguments();
		
		ArrayList<Content> list = (ArrayList<Content>) args.getSerializable("data");
		
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
//				getActivity(), android.R.layout.simple_list_item_1, list);
		
		ListView list1 = (ListView) rootView.findViewById(R.id.list); 
		ListAdapter adapter = new ListAdapter(getActivity(), R.layout.custom_listview, list);
		list1.setAdapter(adapter);
		
		return rootView;
	}
	
}
