package com.example.viewpager.adapter;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;

import com.example.viewpager.Content;
import com.example.viewpager.R;
import com.example.viewpager.fragment.MyFragment;

public class MyAdapter extends FragmentPagerAdapter {
	Content[] cs;

	public MyAdapter(FragmentManager fm, Content[] cs) {
		super(fm);
		this.cs = cs;
	}

	// 보여지는 프래그먼트 생성
	@Override
	public Fragment getItem(int position) {
		MyFragment fragment = new MyFragment();
		Bundle args = new Bundle();
//		args.putSerializable("data", cs[position]);
		ArrayList<Content> moneyList = new ArrayList<Content>();
		if(position == 0) {
			
			moneyList.add(new Content("후문1", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문2", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문3", R.drawable.ic_launcher, "버튼1"));
		} else if(position == 1) {
			moneyList.add(new Content("후문4", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문5", R.drawable.ic_launcher, "버튼1"));
			moneyList.add(new Content("후문6", R.drawable.ic_launcher, "버튼1"));
		} else {
			moneyList.add(new Content("까동", R.drawable.ggadong, "버튼1"));
			moneyList.add(new Content("벼락", R.drawable.byeorak, "버튼1"));
			moneyList.add(new Content("알촌", R.drawable.alchon, "버튼1"));
			moneyList.add(new Content("도스마스", R.drawable.dosmas, "버튼1"));
		}
		args.putSerializable("data", moneyList);
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public int getCount() {
		return cs.length;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return null;
	}

	
}
