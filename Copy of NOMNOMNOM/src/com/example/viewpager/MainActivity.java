package com.example.viewpager;

import java.util.Locale;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;


import com.example.viewpager.adapter.MyAdapter;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v13.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends Activity implements ActionBar.TabListener {

	MyAdapter mSectionsPagerAdapter;

	ViewPager mViewPager;

	MenuDrawer mMenuDrawer = null;

	private void initSlidingMenu() {

		mMenuDrawer = MenuDrawer.attach(this, MenuDrawer.Type.BEHIND,
				Position.LEFT, MenuDrawer.MENU_DRAG_WINDOW);
		mMenuDrawer.setMenuView(R.layout.sliding_menu);
		mMenuDrawer.setContentView(R.layout.activity_main);
		mMenuDrawer.setDrawerIndicatorEnabled(true);
		mMenuDrawer.setTouchMode(MenuDrawer.TOUCH_MODE_BEZEL);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initSlidingMenu();

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// 각 프래그먼트에서 보여질 내용(배열)
		Content[] cs = { new Content("빚진놈들", R.drawable.ic_launcher, "버튼1"),
				new Content("갚을놈들", R.drawable.ic_launcher, "버튼2"),
				new Content("통계", R.drawable.ic_launcher, "버튼3"),
		new Content("지난기록들", R.drawable.ic_launcher, "버튼4")};

		mSectionsPagerAdapter = new MyAdapter(getFragmentManager(), cs);

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// 탭의 제목 생성
		for (int i = 0; i < cs.length; i++) {
			String title = cs[i].getTitle();

			actionBar.addTab(actionBar.newTab().setText(title)
					.setTabListener(this));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

}
