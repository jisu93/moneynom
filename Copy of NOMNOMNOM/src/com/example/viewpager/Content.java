package com.example.viewpager;

import java.io.Serializable;

public class Content implements Serializable {
	String Name;
	int image;
	String btn;

	public String getBtn() {
		return btn;
	}

	public void setBtn(String btn) {
		this.btn = btn;
	}

	public Content(String title, int image, String btn) {
		this.Name = title;
		this.image = image;
		this.btn = btn;
	}

	public String getTitle() {
		return Name;
	}

	public void setTitle(String title) {
		this.Name = title;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

}
