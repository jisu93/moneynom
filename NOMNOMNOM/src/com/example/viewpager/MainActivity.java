package com.example.viewpager;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.sax.RootElement;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.viewpager.adapter.MyAdapter;

public class MainActivity extends Activity implements ActionBar.TabListener {

	MyAdapter mSectionsPagerAdapter;

	ViewPager mViewPager;

	MenuDrawer mMenuDrawer = null;

	private void initSlidingMenu() {

		mMenuDrawer = MenuDrawer.attach(this, MenuDrawer.Type.BEHIND,
				Position.LEFT, MenuDrawer.MENU_DRAG_WINDOW);
		mMenuDrawer.setMenuView(R.layout.sliding_menu);
		mMenuDrawer.setContentView(R.layout.activity_main);
		mMenuDrawer.setDrawerIndicatorEnabled(true);
		mMenuDrawer.setTouchMode(MenuDrawer.TOUCH_MODE_BEZEL);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initSlidingMenu();

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// 각 프래그먼트에서 보여질 내용(배열)
		Content[] cs = { new Content("빚진놈들", "버튼1"),
				new Content("빚쟁이들", "버튼2"),
			};

		mSectionsPagerAdapter = new MyAdapter(getFragmentManager(), cs);

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// 탭의 제목 생성
		for (int i = 0; i < cs.length; i++) {
			String title = cs[i].getTitle();

			actionBar.addTab(actionBar.newTab().setText(title)
					.setTabListener(this));
		}
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

}
