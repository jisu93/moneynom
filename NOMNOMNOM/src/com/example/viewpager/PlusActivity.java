package com.example.viewpager;


import java.util.ArrayList;
import java.util.zip.Inflater;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PlusActivity extends Activity {
	myDBHelper myHelper;
	SQLiteDatabase sqlDB;
	EditText setName, setMoney;
	TextView textName, textMoney;
	public ArrayList<Content> list;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plus);
		
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = getLayoutInflater().inflate(R.layout.custom_listview, null, false);
		
		Button btnDelete = (Button) findViewById(R.id.buttDelete);
		Button btnCancel = (Button) findViewById(R.id.buttX);
		Button btnComplete = (Button) findViewById(R.id.buttComplete);
		setName = (EditText) findViewById(R.id.setName);
		setMoney = (EditText) findViewById(R.id.setMoney);
//		textName = findViewById()
		list = new ArrayList<Content>();
		
		
		btnComplete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String sname = setName.getText().toString();
				String smoney = setMoney.getText().toString();
				
				list.add(new Content(sname, smoney));
				
				Intent intent = new Intent(PlusActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
	}
	public class myDBHelper extends SQLiteOpenHelper {
		public myDBHelper(Context context) {
			super(context, "groupDB", null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE  groupTBL ( gName CHAR(20) PRIMARY KEY, gNumber INTEGER);");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS groupTBL");
			onCreate(db);
		}
	}
}
